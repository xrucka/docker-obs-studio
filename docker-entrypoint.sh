#!/bin/bash

if [[ "$1" = "bash" ]] || [[ -x "$1" ]] ; then
	exec "$@"
fi

profiledir=".config/obs-studio/basic/profiles"
tmpprofile="${profiledir}/tmp-$(date +%s)"

args=()
while test $# -gt 0 ; do
	cloneprofile=""
	if [[ "$1" = "--from" ]] ; then
		shift
		cloneprofile="$1"
	elif [[ -d "$profiledir/$1" ]] ; then
		cloneprofile="$1"
	elif [[ "$1" = "--out" ]] || [[ "$1" = "--output" ]] ; then
		if ! [[ -e "$tmpprofile/basic.ini" ]] ; then
			echo "Temporary profile not established!" 1>&2
			exit 1
		fi

		shift
		echo sed -ri "s#^FilenameFormatting=.*\$#FilenameFormatting=$1#g" "$tmpprofile/basic.ini"
		sed -ri "s#^FilenameFormatting=.*\$#FilenameFormatting=$1#g" "$tmpprofile/basic.ini"
	else 
		args+=("$1")
	fi

	if [[ "$cloneprofile" != "" ]] ; then
		cp -r "$profiledir/$cloneprofile" "$tmpprofile"
		sed -ri "s#^Name=.*\$#Name=$(basename "$tmpprofile")#g" "$tmpprofile/basic.ini"
		args+=("--profile" "$(basename "$tmpprofile")")
	fi
	shift
done

cat "$tmpprofile/basic.ini"

echo obs "${args[@]}"
obs "${args[@]}"
rm -rf "$tmpprofile"
