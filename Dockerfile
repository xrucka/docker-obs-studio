FROM debian:buster

RUN apt-get update && apt-get install -y wget strace x11-xserver-utils obs-studio pulseaudio-utils pulsemixer pavucontrol

ARG UID=21702
ARG VIDEO_GID=33
ARG AUDIO_GID=17
ARG XDG_RUNTIME_DIR=/run/user/21702
ARG USER=luksoft


COPY docker-entrypoint.sh /
RUN groupmod -g ${AUDIO_GID} audio \
	&& userdel www-data \
	&& groupmod -g {VIDEO_GID} video \
	&& useradd -u $(basename ${XDG_RUNTIME_DIR}) -G ${AUDIO_GID},${VIDEO_GID} -m -s /bin/bash $USER \
	&& chmod +x /docker-entrypoint.sh

ENV DISPLAY=:0
ENV XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR}

WORKDIR /home/$USER
USER $USER

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [  ]
#    --env="DISPLAY" \
#    --env="QT_X11_NO_MITSHM=1" \
#    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
